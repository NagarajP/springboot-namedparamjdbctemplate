package com.myzee.service;

import java.util.List;

import com.myzee.entity.Employee;

public interface EmployeeService {

	int create(Employee e);
	public Employee getEmployee(int empId);
	public List<Employee> getAllEmployee();
	public String updateEmployee(int empSalary, int empId);

}
