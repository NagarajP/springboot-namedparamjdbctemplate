package com.myzee.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.myzee.dao.EmployeeDAO;
import com.myzee.entity.Employee;

@Service
public class EmployeeServiceImpl implements EmployeeService {

	@Autowired
	EmployeeDAO employeeDAO;
	
	@Override
	public int create(Employee e) {
		
		int recordCreated = employeeDAO.create(e);
		return recordCreated;
	}

	@Override
	public Employee getEmployee(int empId) {
		return employeeDAO.getEmployee(empId);
	}

	@Override
	public List<Employee> getAllEmployee() {
		return employeeDAO.getAllEmployee();
	}

	@Override
	public String updateEmployee(int empSalary, int empId) {
		return employeeDAO.updateEmployee(empSalary, empId);
	}
	
}
