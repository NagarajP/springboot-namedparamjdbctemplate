package com.myzee;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbootNamedparamjdbctemplateApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringbootNamedparamjdbctemplateApplication.class, args);
	}

}
