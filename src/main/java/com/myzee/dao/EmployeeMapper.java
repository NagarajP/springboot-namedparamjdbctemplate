package com.myzee.dao;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.myzee.entity.Employee;

public class EmployeeMapper implements RowMapper<Employee> {

	@Override
	public Employee mapRow(ResultSet rs, int rowNum) throws SQLException {
		Employee emp = new Employee();
		emp.setEmpId(rs.getInt("emp_id"));
		emp.setEmpName(rs.getString("emp_name"));
		emp.setEmpSalary(rs.getInt("emp_salary"));
		
		return emp;
	}

}
