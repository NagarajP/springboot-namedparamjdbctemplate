package com.myzee.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Component;

import com.myzee.entity.Employee;

@Component
public class EmployeeDAOImpl implements EmployeeDAO {

	@Autowired
	NamedParameterJdbcTemplate template;

	@Override
	public int create(Employee e) {
		String sql = "insert into Employee(emp_id, emp_name, emp_salary) values (:empId, :empName, :empSalary)";
//		SqlParameterSource namedParameters = new MapSqlParameterSource();
		Map<String, Object> namedParameters = new HashMap<>();

//		KeyHolder keyHolder = new GeneratedKeyHolder();

		namedParameters.put("empId", e.getEmpId());
		namedParameters.put("empName", e.getEmpName());
		namedParameters.put("empSalary", e.getEmpSalary());

		int recordCreated = template.update(sql, namedParameters);
//		template.update(sql, namedParameters, keyHolder);
		System.out.println("Record Created : " + recordCreated);
		return recordCreated;
	}

	@Override
	public Employee getEmployee(int empId) {

		String sql = "select * from Employee where emp_id = :empId";
		SqlParameterSource namedParam = new MapSqlParameterSource("empId", empId);
		Employee e = template.queryForObject(sql, namedParam, new EmployeeMapper());
		return e;
	}

	@Override
	public List<Employee> getAllEmployee() {
		String sql = "select * from Employee";
		return template.query(sql, new EmployeeMapper());
	}

	@Override
	public String updateEmployee(int empSalary, int empId) {

		String SQL = "UPDATE Employee SET emp_salary = :empSalary WHERE emp_id = :empId";
		MapSqlParameterSource namedParameters = new MapSqlParameterSource();
		namedParameters.addValue("empSalary", empSalary);
		namedParameters.addValue("empId", empId);
		template.update(SQL, namedParameters);
		System.out.println("Updated Record with EMPID = " + empId);

		return empId + " got updated";
	}

}
