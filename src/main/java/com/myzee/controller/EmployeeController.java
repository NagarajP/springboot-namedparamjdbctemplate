package com.myzee.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.myzee.entity.Employee;
import com.myzee.service.EmployeeService;

@RestController
@RequestMapping(value = "/emp")
public class EmployeeController {
	
	@Autowired
	EmployeeService employeeService;
	
	@PostMapping(value = "/add", consumes = "application/json")
	public String create(@RequestBody Employee e) {
		
		int recordCreated = employeeService.create(e);
		if(recordCreated == 0)
			return "No record inserted";
		else 
			return "Record Created Successfully!!";
	}
	
	@GetMapping(value = "/get/{empId}", produces = "application/json")
	public Employee getEmployee(@PathVariable("empId") int empId) {
		return employeeService.getEmployee(empId);
	}
	
	@GetMapping(value = "/get/all", produces = "application/json")
	public List<Employee> getAllEmployees(){
		return employeeService.getAllEmployee();
	}
	
	@PutMapping(value = "/update/{empSalary}/{empId}")
	public String updateEmployee(@PathVariable("empSalary") int empSalary, 
			@PathVariable("empId") int empId) {
		return employeeService.updateEmployee(empSalary, empId);
	}
	
}
